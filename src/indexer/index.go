package indexer

import (
	"log"
	"os"
	"path/filepath"
	"strings"
)

func IndexMedia(directory string) IndexedAlbum {
	index := make(map[string]*IndexedAlbum)

	mediaRoot := filepath.Clean(directory) + string(filepath.Separator)

	var rootAlbum *IndexedAlbum = nil
	var currentAlbum *IndexedAlbum = nil

	err := filepath.Walk(mediaRoot, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		fsEntity := FSEntity{
			Path:     filepath.ToSlash(strings.TrimPrefix(path, mediaRoot)),
			FilePath: path,
			Name:     strings.TrimSuffix(info.Name(), filepath.Ext(info.Name())),
		}

		if info.IsDir() {
			// Found album

			if currentAlbum != nil && currentAlbum.Entity.Path != "" {
				if parent, ok := index[filepath.ToSlash(filepath.Dir(currentAlbum.Entity.Path))]; ok {
					// Parent album found, so save as a sub-album of that album
					parent.Albums[currentAlbum.Entity.Path] = *currentAlbum
				} else {
					// No parent album, so save as a sub-album of the root album
					rootAlbum.Albums[currentAlbum.Entity.Path] = *currentAlbum
				}

				// Add current album to index
				index[currentAlbum.Entity.Path] = currentAlbum
			}

			// Create new album
			currentAlbum = &IndexedAlbum{
				Entity: fsEntity,
				Albums: make(map[string]IndexedAlbum),
				Media:  make(map[string]IndexedMedia),
			}

			// First album, so save it as the root
			if rootAlbum == nil {
				rootAlbum = currentAlbum
			}
		} else {
			// Found media, add to current album
			currentAlbum.Media[fsEntity.Path] = IndexedMedia{
				Entity: fsEntity,
			}
		}

		return nil
	})

	if err != nil {
		log.Fatalln(err)
	}

	return *rootAlbum
}

func PrintIndex(rootAlbum *IndexedAlbum) {
	var printIndex func(index *IndexedAlbum, level int)

	printIndex = func(album *IndexedAlbum, level int) {
		log.Println(strings.Repeat("\t", level), album)

		for _, album := range album.Albums {
			printIndex(&album, level+1)
		}

		for _, media := range album.Media {
			log.Println(strings.Repeat("\t", level), "    ", &media)
		}
	}

	printIndex(rootAlbum, 0)
}
