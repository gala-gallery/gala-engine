package generator

import "strings"

type Gallery struct {
	Title           string
	ThumbnailWidth  int    // Width, in pixels, of thumbnail images
	ThumbnailHeight int    // Height, in pixels, of thumbnail images
	Template        string // Location of template
}

type Album struct {
	Name   string  // Name of the album to display
	Path   string  // Path relative to gallery root
	Albums []Album // List of sub-albums
	Media  []Media // List of media
}

type Media struct {
	Name      string // Name of the media to display
	Path      string // Path relative to gallery root
	Thumbnail string // Location of the thumbnail image relative to the gallery root
}

type TemplateData struct {
	Gallery      *Gallery    // Gallery options
	Item         interface{} // Album or Media
	RelativeRoot string      // Relative path to the root
}

func DataForTemplate(gallery *Gallery, item interface{}, level int) TemplateData {
	return TemplateData{
		Gallery:      gallery,
		Item:         item,
		RelativeRoot: strings.Repeat("../", level),
	}
}
